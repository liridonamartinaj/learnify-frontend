export interface Course {
    id?: String;
    name: String;
    type: String;
}


export interface Booking {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  courseId: number;
}
