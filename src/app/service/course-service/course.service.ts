import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environment/environment";
import {Booking} from "../../models/course";

@Injectable({
  providedIn: 'root'
})
export class CourseService {
  private searchObservable: BehaviorSubject<string> = new BehaviorSubject<string>('');

  get searchObservable$() {
    return this.searchObservable.asObservable();
  }

  constructor(private httpClient: HttpClient) { }

  setQuery(query: string) {
    this.searchObservable.next(query);
  }

  getCourses(query?: string) {
    return this.httpClient.get<any[]>(environment.apiUrl + "courses/list?query=" + (query || ''));
  }

  getCourse(id: any) {
    return this.httpClient.get<any[]>(environment.apiUrl + "courses/detail?id=" + (id || ''));
  }

  bookCourse(booking: Booking) {
    return this.httpClient.post<any>(environment.apiUrl + "courses/book", booking);
  }
}
