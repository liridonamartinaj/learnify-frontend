import {Component, Input} from '@angular/core';
import {CourseService} from "../../../service/course-service/course.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent {
  @Input("query") query?: string

  constructor(private courseService: CourseService, protected router: Router) {
  }

  doSearch() {
    this.router.navigateByUrl("/courses").finally(() => this.courseService.setQuery(this.query ?? ''))
  }
}
