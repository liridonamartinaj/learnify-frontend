import {Component, HostListener} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  isSticky: boolean = false;

  @HostListener('window:scroll', ['$event']) checkScroll() {
    this.isSticky = window.scrollY >= 1;
  }

  constructor(public router: Router) {
  }
}
