import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {RouterOutlet} from "@angular/router";
import { HomePageComponent } from './pages/home-page/home-page.component';
import { CourseDetailsPageComponent } from './pages/course-details-page/course-details-page.component';
import { CoursesPageComponent } from './pages/courses-page/courses-page.component';
import { ContactUsPageComponent } from './pages/contact-us-page/contact-us-page.component';
import {AppRoutingModule} from "./app.routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from "./shared/header/header/header.component";
import {IconsModule} from "./icons/icons.module";
import {FooterComponent} from "./shared/footer/footer/footer.component";
import {NgxBootstrapIconsModule} from "ngx-bootstrap-icons";
import {allIcons} from "ng-bootstrap-icons/icons";
import {SearchBoxComponent} from "./shared/search-box/search-box/search-box.component";
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    CourseDetailsPageComponent,
    CoursesPageComponent,
    ContactUsPageComponent,
    HeaderComponent,
    FooterComponent,
    SearchBoxComponent
  ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      IconsModule,
      RouterOutlet,
      NgbModule,
      FormsModule,
      HttpClientModule,
      NgxBootstrapIconsModule.pick(allIcons)
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
