import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomePageComponent} from "./pages/home-page/home-page.component";
import {CourseDetailsPageComponent} from "./pages/course-details-page/course-details-page.component";
import {CoursesPageComponent} from "./pages/courses-page/courses-page.component";
import {ContactUsPageComponent} from "./pages/contact-us-page/contact-us-page.component";

const routes: Routes = [
  {path: '', redirectTo:'/home-page', pathMatch:'full'},
  {path:'home-page', component:HomePageComponent},
  {path:'course-details/:id', component:CourseDetailsPageComponent},
  {path:'courses', component:CoursesPageComponent},
  {path:'contact-us', component:ContactUsPageComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
