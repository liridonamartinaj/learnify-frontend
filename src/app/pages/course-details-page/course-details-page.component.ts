import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CourseService} from "../../service/course-service/course.service";
import {Booking} from "../../models/course";

@Component({
  selector: 'app-course-details-page',
  templateUrl: './course-details-page.component.html',
  styleUrls: ['./course-details-page.component.css']
})
export class CourseDetailsPageComponent implements OnInit {
  course?: any;
  loading: boolean = true;
  id?: any
  booking: Booking = {} as Booking;
  message = ""

  constructor(private activateRoute: ActivatedRoute, private courseService: CourseService) {
  }

  ngOnInit(): void {
    this.id = this.activateRoute.snapshot.paramMap.get("id");

    this.loading = true;
    this.courseService.getCourse(this.id ?? '').subscribe({
      next: course => {
        this.course = course
        this.loading = false;
      },
      error: err => {
        console.error(err);
        this.loading = false;
      }
    })
  }


  public doBooking() {
    console.info("called")
    if (!this.booking?.firstName) {
      this.message = "Please insert your first name";
      return
    }

    if (!this.booking?.lastName) {
      this.message =  "Please insert your last name";
      return
    }

    if (!this.booking?.email) {
      this.message =  "Please insert your email";
      return
    }

    if (!this.booking?.phoneNumber) {
      this.message = "Please insert your phone number";
      return
    }
    this.loading = true;
    this.booking.courseId = this.course.id;

    // TODO: Send to the server
    this.courseService.bookCourse(this.booking).subscribe({
      next: response => {
        this.message = response.message;
        this.booking = {} as Booking;
        this.loading = false;
      },
      error: err => {
        console.info("called error")
        this.message = "Something went wrong. Try again or contact administrator!";
        this.loading = false;
      }
    })
  }
}
