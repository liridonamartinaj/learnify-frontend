import {Component, OnDestroy, OnInit} from '@angular/core';
import {CourseService} from "../../service/course-service/course.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-courses-page',
  templateUrl: './courses-page.component.html',
  styleUrls: ['./courses-page.component.css']
})
export class CoursesPageComponent implements OnInit, OnDestroy {
  searchBoxSubscription?: Subscription;
  query?: string;
  items: any[] = [];
  loading: boolean = false;

  constructor(private courseService: CourseService) {
  }

  ngOnInit(): void {
    if (this.searchBoxSubscription)
      this.searchBoxSubscription.unsubscribe();

      this.searchBoxSubscription = this.courseService.searchObservable$.subscribe({
        next: query => {
          this.query = query;
          this.search();
          console.info("Text to search", query);
        }
      });
  }

  ngOnDestroy(): void {
    if(this.searchBoxSubscription) {
      this.searchBoxSubscription.unsubscribe();
      this.searchBoxSubscription = undefined;
    }
  }

  search() {
    this.items = [];
    this.loading = true;
    this.courseService.getCourses(this.query).subscribe({
      next: items => {
        // Update courses list
        this.items = items;
        this.loading = false;
      },
      error: err => {
        console.error(err)
        this.loading = false;
      }
    })

    console.info("Called")
  }
}
